package jp.alhinc.junit_sample;

import static org.junit.Assert.*;

import org.junit.Test;

public class FizzBuzzTest {
	/**
	 * 3の倍数の時
	 */
	@Test
	public void testFizz() {
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	/**
	 * 5の倍数の時
	 */
	@Test
	public void testBuzz() {
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}
	/**
	 * 3と5両方の倍数の時
	 */
	@Test
	public void testFizzBuzz() {
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}
	/**
	 * それ以外の場合
	 */
	@Test
	public void testNum() {
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}
	@Test
	public void testNum2() {
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}
}
